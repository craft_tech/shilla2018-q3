define([
        "controllers/pages/loading",
        "controllers/pages/home",
        "controllers/pages/page1",
        "controllers/pages/page2",
        "controllers/pages/page3",
        "controllers/pages/page4",
        "controllers/pages/page5",
        "controllers/pages/page6",
        "controllers/pages/page7",
        "controllers/pages/page8",
        "controllers/pages/page9",
        "controllers/pages/page10",
        "controllers/pages/page11",
        "controllers/pages/page12",
        "controllers/pages/result1",
        "controllers/pages/result2",
        "controllers/pages/result3",
        "controllers/pages/result4",
        "controllers/pages/result5",
        "controllers/pages/guide1",
        "controllers/pages/guide2",
        "controllers/pages/guide3",
        "controllers/pages/guide4",
        "controllers/pages/guide5"
    ],
    function(Loading, Home, Page1, Page2, Page3, Page4, Page5, Page6, Page7, Page8, Page9, Page10, Page11, Page12, Result1, Result2, Result3, Result4, Result5, Guide1, Guide2, Guide3, Guide4, Guide5) {
        var pages = [
            {
                routeId: 'loading',
                type: 'main',
                landing: true,
                page: function() {
                    return new Loading.View({ template: "pages/loading" });
                }
            },
            {
                routeId: 'home',
                type: 'main',
                landing: false,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'page1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page1.View({ template: "pages/page1" });
                }
            },
            {
                routeId: 'page2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page2.View({ template: "pages/page2" });
                }
            },
            {
                routeId: 'page3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page3.View({ template: "pages/page3" });
                }
            },
            {
                routeId: 'page4',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page4.View({ template: "pages/page4" });
                }
            },
            {
                routeId: 'page5',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page5.View({ template: "pages/page5" });
                }
            },
            {
                routeId: 'page6',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page6.View({ template: "pages/page6" });
                }
            },
            {
                routeId: 'page7',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page7.View({ template: "pages/page7" });
                }
            },
            {
                routeId: 'page8',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page8.View({ template: "pages/page8" });
                }
            },
            {
                routeId: 'page9',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page9.View({ template: "pages/page9" });
                }
            },
            {
                routeId: 'page10',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page10.View({ template: "pages/page10" });
                }
            },
            {
                routeId: 'page11',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page11.View({ template: "pages/page11" });
                }
            },
            {
                routeId: 'page12',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page12.View({ template: "pages/page12" });
                }
            },
            {
                routeId: 'result1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result1.View({ template: "pages/result1" });
                }
            },
            {
                routeId: 'result2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result2.View({ template: "pages/result2" });
                }
            },
            {
                routeId: 'result3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result3.View({ template: "pages/result3" });
                }
            },
            {
                routeId: 'result4',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result4.View({ template: "pages/result4" });
                }
            },
            {
                routeId: 'result5',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result5.View({ template: "pages/result5" });
                }
            },
            {
                routeId: 'guide1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide1.View({ template: "pages/guide1" });
                }
            },
            {
                routeId: 'guide2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide2.View({ template: "pages/guide2" });
                }
            },
            {
                routeId: 'guide3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide3.View({ template: "pages/guide3" });
                }
            },
            {
                routeId: 'guide4',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide4.View({ template: "pages/guide4" });
                }
            },
            {
                routeId: 'guide5',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide5.View({ template: "pages/guide5" });
                }
            },
        ];
        return pages;
    });