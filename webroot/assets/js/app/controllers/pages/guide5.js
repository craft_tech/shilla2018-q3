// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {

                var context = this;

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.guide5_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                    cloudsDrift5();
                    titleSlideIn5();
                } }, 0.1);

                // 标题滑入
                function titleSlideIn5() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.title1'), 0.8, { 'left': '0%' }, 0);
                    tl.to(context.$('.title2'), 0.8, { 'right': '0%' }, 0);
                    tl.to(context.$('.title3'), 0.8, { 'left': '0%' }, 0.2);
                    tl.to(context.$('.title4'), 0.8, { 'right': '0%' }, 0.2);
                };

                // 云朵飘动
                function cloudsDrift5() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.cloud1'), 4, { 'left': '62.6%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0);
                    tl.to(context.$('.cloud2'), 7, { 'left': '74%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0.3);
                };

                // 寻找拍档
                $('.btn-share').on('click', function() {
                    gtag('event', 'search_SINGAPORE', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.to(context.$('.modal'), 0.2, { 'display': 'block', autoAlpha: 1 });
                });

                // 再测一次
                $('.btn-again').on('click', function() {
                    gtag('event', 'test', {'event_category': 'shilla2018Q3', 'event_label': 'click'});
                    tl.kill();
                    app.router.goto('home');
                    app.qArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                    app.result = '';
                });
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })