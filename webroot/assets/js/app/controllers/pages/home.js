// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {

                var context = this;

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.home_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                    titleSlideIn();
                    cloudsDrift();
                    wheel();
                } }, 0.1);

                // 标题滑入
                function titleSlideIn() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.title1'), 0.8, { 'left': 0 });
                    tl.to(context.$('.title2'), 0.8, { 'left': 0 }, '-=0.8');
                };

                // 云朵飘动
                function cloudsDrift() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.cloud1'), 4, { 'left': '0%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0);
                    tl.to(context.$('.cloud2'), 2, { 'left': '14.4%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0.5);
                    tl.to(context.$('.cloud3'), 4, { 'left': '46.5%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 1);
                    tl.to(context.$('.cloud4'), 3, { 'left': '80.8%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0.5);
                };

                // 摩天轮转动
                function wheel() {
                    var tl = new TimelineMax({ repeat: -1});
                    tl.to(context.$('.wheel'), 400, { rotation:3600, transformOrigin:"center center" });
                };

                // 马上测试
                $('.btn-start').on('click', function() {
                    gtag('event', 'start', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    var qNum = app.randomQuestion();
                    app.router.goto('page' + app.qArr[qNum]);
                    app.qArr.splice(qNum, 1);
                });
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })