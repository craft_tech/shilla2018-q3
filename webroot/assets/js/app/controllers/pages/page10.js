// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {

                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/bg.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/title1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/title2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/title3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/title4.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/icon1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/icon2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/icon3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/icon4.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/save.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result5/btn-guide.png",
                            "size": 1
                        },
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                    }
                });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.page10_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                    dotTwinkle();
                    optionShake();
                } }, 0.1);

                // 提示
                function dotTwinkle() {
                    var tl = new TimelineMax({ repeat: -1, repeatDelay: 0.3 });
                    tl.to(context.$('.dot1'), 0.3, { autoAlpha: 1 });
                    tl.to(context.$('.dot2'), 0.3, { autoAlpha: 1 }, '+=0.1');
                    tl.to(context.$('.dot3'), 0.3, { autoAlpha: 1 }, '+=0.1');
                    tl.to(context.$('.dot'), 0.3, { autoAlpha: 0 }, '+=0.1');
                };

                // 选项晃动
                function optionShake() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.btn-city'), 0.5, { 'left': '9.9%', repeat: -1, yoyo: true, ease: Power0.easeNone }, 0);
                    tl.to(context.$('.btn-island'), 0.4, { 'left': '40.1%', repeat: -1, yoyo: true, ease: Power0.easeNone }, 0.15);
                    tl.to(context.$('.btn-mix'), 0.5, { 'left': '68.7%', repeat: -1, yoyo: true, ease: Power0.easeNone }, 0);
                };

                // 选时髦都市游
                $('.btn-city').on('click', function() {
                    gtag('event', 'city', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    app.qArr.push(11);
                    if(app.qArr.length > 0) {
                        var qNum = app.randomQuestion();
                        app.router.goto('page' + app.qArr[qNum]);
                        app.qArr.splice(qNum, 1);
                    } else {
                        app.router.goto('result' + app.result);
                    } 
                });

                // 选浪漫海岛游
                $('.btn-island').on('click', function() {
                    gtag('event', 'island', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    app.qArr.push(12);
                    if(app.qArr.length > 0) {
                        var qNum = app.randomQuestion();
                        app.router.goto('page' + app.qArr[qNum]);
                        app.qArr.splice(qNum, 1);
                    } else {
                        app.router.goto('result' + app.result);
                    } 
                });
                // 选半城半海
                $('.btn-mix').on('click', function() {
                    gtag('event', 'mix', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    app.result = 5;
                    if(app.qArr.length > 0) {
                        var qNum = app.randomQuestion();
                        app.router.goto('page' + app.qArr[qNum]);
                        app.qArr.splice(qNum, 1);
                    } else {
                        app.router.goto('result' + app.result);
                    } 
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
        })
        //  Return the module for AMD compliance.
        return Page;
    })
