// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/bg-light.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/btn-share.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/btn-again.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/modal-content.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/scenery.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/title.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/title1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/title2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/title3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide4/title4.png",
                            "size": 1
                        }
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                    }
                });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.result4_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                    iconsShake4(context.$('.icon1'), 'rotate15', 0, 0.2, 0.2);
                    iconsShake4(context.$('.icon2'), 'rotate-25', 0.2, 0.4, 0.2);
                    iconsShake4(context.$('.icon3'), 'rotate-15', 0.1, 0.3, 0.2);
                    titlesSlideIn4();
                } }, 0.1);

                // 图标滑入
                function titlesSlideIn4() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.title1'), 0.8, { 'left': '0%' }, 0);
                    tl.to(context.$('.title6'), 0.8, { 'right': '0%' }, 0);
                    tl.to(context.$('.title2'), 0.7, { 'right': '0%' }, 0.2);
                    tl.to(context.$('.title4'), 0.8, { 'left': '0%' }, 0.2);
                    tl.to(context.$('.title3'), 0.7, { 'right': '0%' }, 0.4);
                    tl.to(context.$('.title5'), 0.8, { 'left': '0%' }, 0.4);
                };

                // 图标晃动
                function iconsShake4(el, className, start, end, delay) {
                    var tl = new TimelineMax({repeat: -1, repeatDelay: delay });
                    tl.to(el, 0, { className: '+=' + className }, start);
                    tl.to(el, 0, { className: '-=' + className }, end);
                };

                // 攻略
                context.$('.btn-guide').on('click', function() {
                    gtag('event', 'apply_PHUKET', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    app.router.goto('guide4');  
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
        })
        //  Return the module for AMD compliance.
        return Page;
    })
