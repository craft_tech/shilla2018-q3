// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                // 获取授权
                // app.localUrl = window.location.href;
                // if(app.localUrl.indexOf('openid') < 0) {
                //     location.href = app.baseUrl + '/interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(app.localUrl);
                // };
                // app.openid = wxr_getParameterByName('openid');

                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "AUDIO",
                            "sources": {
                                "mp3": {
                                    "source": "assets/audio/music.mp3",
                                    "size": 1
                                }
                            },
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/bg.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/building.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/wheel.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/bg.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/bg-light.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/btn-yes.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/btn-no.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/tv1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/tv2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/tv3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/dot1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/dot2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/dot3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q4.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q5.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q6.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q7.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q8.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q9.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q10.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q11.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/pages/q12.png",
                            "size": 1
                        }
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                    }
                });

                var context = this;

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.loading_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                    loading();
                } }, 0.1);
                tl.to(context.$('.loading_page'), 0, { onComplete: function() {
                    tl.kill();
                    app.router.goto('home');
                } }, '+=1.5');

                // 进度条
                function loading() {
                    var tl = new TimelineMax({ });
                    var random1 = Math.floor((Math.random() * 10) + 1);
                    var random2 = Math.floor((Math.random() * 10) + 11);
                    var random3 = Math.floor((Math.random() * 10) + 21);
                    var random4 = Math.floor((Math.random() * 10) + 31);
                    var random5 = Math.floor((Math.random() * 10) + 51);
                    var random6 = Math.floor((Math.random() * 10) + 81);
                    $('.num').html(random1);
                    tl.to(context.$('.bar-red'), 0.1, { 'width': random1 + '%' });
                    setTimeout(function() {
                        $('.num').html(random2);
                        tl.to(context.$('.bar-red'), 0.1, { 'width': random2 + '%' });
                    }, 100)
                    setTimeout(function() {
                        $('.num').html(random3);
                        tl.to(context.$('.bar-red'), 0.1, { 'width': random3 + '%' });
                    }, 200)
                    setTimeout(function() {
                        $('.num').html(random4);
                        tl.to(context.$('.bar-red'), 0.1, { 'width': random4 + '%' });
                    }, 300)
                    setTimeout(function() {
                        $('.num').html(random5);
                        tl.to(context.$('.bar-red'), 0.1, { 'width': random5 + '%' });
                    }, 400)
                    setTimeout(function() {
                        $('.num').html(random6);
                        tl.to(context.$('.bar-red'), 0.1, { 'width': random6 + '%' });
                    }, 500)
                    setTimeout(function() {
                        $('.num').html('100');
                        tl.to(context.$('.bar-red'), 0.1, { 'width': '100%' });
                    }, 600);
                };
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })